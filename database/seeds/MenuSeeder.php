<?php

use App\Menu;
use App\MenuCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{

    public function run()
    {
        DB::table('menu_categories')->delete();
        DB::table('menus')->delete();

        $categories = [
            [
                'name' => 'Appetizer',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Beef',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Pork',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Chicken',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Kambing',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Fish',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Vegetable',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Soup',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Dessert',
                'for' => 'catering',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Chibogan',
                'for' => 'table',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Sizzling Platters',
                'for' => 'table',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Barato Paborito',
                'for' => 'table',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        MenuCategory::insert($categories);

        $menus = [
            [
                'menu_category_id' => 1,
                'name' => 'Lumpya',
                'description' => 'Ground Pork with vegetables wrapped with lumpya wrapper',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 2,
                'name' => 'Caldereta',
                'description' => 'Beef bones with tomato sauce, green peas, potato, pineapple chunks and carrots',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 3,
                'name' => 'AFRITADA',
                'description' => 'Pork with tomato sauce, green peas, potato, pineapple chunks and carrots',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 4,
                'name' => 'Sweet Chilli Chix',
                'description' => 'Chix coated with flour then sauted with BBQ sauce',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 5,
                'name' => 'Kaldereta',
                'description' => 'Goat bones with tomato sauce, green peas, potato, pineapple chunks and carrots',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 6,
                'name' => 'Eschabeche',
                'description' => 'Fish coated with flour and seasoned with sweet and sour sauce',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 7,
                'name' => 'Chop Suey',
                'description' => 'Mix vegetable sayuti, string beans, mushroom, chitcharo, sweet corn seasoned with chicken broth',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 8,
                'name' => 'Fish tinola/Sinigang',
                'description' => '',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'menu_category_id' => 9,
                'name' => 'Mango Float',
                'description' => '',
                'price' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Menu::insert($menus);
    }

} 