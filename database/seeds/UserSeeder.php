<?php

use App\Profile;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        $user = [
            [
                'email'     => 'jdoe@gmail.com',
                'password'  => bcrypt('123456'),
                'slug'      => Str::slug(1 . ' ' . 'john doe'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'email'     => 'xennex2014@gmail.com',
                'password'  => bcrypt('123456'),
                'slug'      => Str::slug(2 . ' ' . 'xen nex'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];
        User::insert($user);
    }

} 