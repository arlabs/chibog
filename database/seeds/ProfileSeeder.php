<?php

use App\Profile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{

    public function run()
    {
        DB::table('profiles')->delete();
        $profiles = [
            [
                'user_id' => 1,
                'full_name' => 'John Doe',
                'mobile' => '09283728374',
                'address' => 'Auckland',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'user_id' => 2,
                'full_name' => 'Xen Nex',
                'mobile' => '09262140562',
                'address' => 'washington',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Profile::insert($profiles);
    }

} 