<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('menu_category_id')->unsigned()->index();
            //$table->foreign('menu_category_id')->references('id')->on('menu_categories')->onDelete('cascade');

            $table->string('name');
            $table->text('description');
            $table->float('price');
            $table->string('image')->nullable();
            $table->boolean('best_seller');
			$table->timestamps();
            $table->softDeletes();
		});

        Schema::create('menu_reservation', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('menu_id')->unsigned()->index();
            $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');

            $table->integer('reservation_id')->unsigned()->index();
            $table->foreign('reservation_id')->references('id')->on('reservations')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('menu_reservation');
		Schema::drop('menus');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
