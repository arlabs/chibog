<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservations', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('reference_code');
            $table->string('type');
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->integer('persons');
            $table->enum('set', ['A','B']);
            $table->float('total');
            $table->enum('status', ['pending','confirmed','cancelled']);
            $table->text('notes');
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('reservations');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
