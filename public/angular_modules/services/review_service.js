(function() {

    'use strict';

    angular.module('app')
        .factory('ReviewService', ReviewService);

    ReviewService.$inject = ['$http'];

    function ReviewService($http)
    {
        var Service = {};

        Service.getSessionData = function()
        {
            return $http({
                url: '/reservations/api/get_session_data',
                method: 'GET'
            });
        };

        Service.submitDetails = function(data)
        {
            return $http({
                url: '/reservations/post_reserve',
                method: 'POST',
                data: data
            });
        };

        return Service;
    }

})();