(function() {

    'use strict';

    angular.module('app')
        .factory('CheckDateService', CheckDateService);

    CheckDateService.$inject = ['$http','$rootScope'];

    function CheckDateService($http, $rootScope) {

        var Service = {};

        Service.checkDate = function(data)
        {
            return $http({
                url: '/reservations/api/check_date',
                method: 'POST',
                data: {date: data.date, hours: data.hours, _token: $rootScope._token}
            });
        };

        Service.submitDetails = function(data)
        {
            return $http({
                url: '/reservations/api/check_date_submit',
                method: 'POST',
                data: data
            });
        };

        return Service;
    }

})();