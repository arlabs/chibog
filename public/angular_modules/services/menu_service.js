(function() {

    'use strict';

    angular.module('app')
        .factory('MenuService', MenuService);

    MenuService.$inject = ['$http', '$rootScope'];

    function MenuService($http, $rootScope)
    {
        var Service = {};

        Service.getAllMenus = function(type)
        {
            return $http({
                url: '/menus/api/get_all?type=' + type,
                method: 'GET'
            });
        };

        Service.getMenus = function(ids)
        {
            return $http({
                url: '/menus/api/get_menus',
                method: 'POST',
                data: {ids: ids, _token: $rootScope._token}
            });
        };

        Service.submitDetails = function(menu_ids)
        {
            return $http({
                url: '/menus/api/submit_ids',
                method: 'POST',
                data: {ids: menu_ids, _token: $rootScope._token}
            });
        };

        return Service;
    }

})();