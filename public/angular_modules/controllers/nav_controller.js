(function() {

    'use strict';

    angular.module('app')
        .controller('NavController', NavController);

    NavController.$inject = ['$scope','$location'];

    function NavController($scope, $location)
    {
        var self = this;

        self.pathMatches = function(path)
        {
            return path === $location.path() ? 'active' : '';
        };

        /*$scope.path_order = 1;

        self.getPath = function(path_order)
        {
            if (path_order > $scope.path_order)
            {
                return '#';
            }
            else
            {
                return '#/step-' + path_order;
            }
        };*/
    }

})();