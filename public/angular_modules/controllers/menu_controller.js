(function() {

    'use strict';

    angular.module('app')
        .controller('MenuController', MenuController);

    MenuController.$inject = ['$scope', 'MenuService', '$location', '$rootScope'];

    function MenuController($scope, MenuService, $location, $rootScope)
    {
        var self = this;

        self.index_num = 0;

        self.menu_ids = [];

        MenuService.getAllMenus($rootScope.type)
            .success(function(response) {
                self.categories = response.categories;

                $scope.unblock_ui();
            })
            .error(function(response) {
                console.log(response);
            });

        self.toggleItem = function(menu_id)
        {
            var ndx = self.menu_ids.indexOf(menu_id);

            if (ndx === 0 || ndx === 1)
            {
                // It exists, remove.
                self.removeItem(menu_id);

                $('#btn-' + menu_id).html('Select').removeClass('btn-primary').addClass('btn-default');
            }
            else
            {
                // It doesn't exists, add.
                self.addItem(menu_id);

                $('#btn-' + menu_id).html('Remove').removeClass('btn-default').addClass('btn-primary');
            }

            console.log(self.menu_ids);
        };

        self.addItem = function(menu_id)
        {
            self.menu_ids.push(menu_id);
        };

        self.removeItem = function(menu_id)
        {
            self.menu_ids.splice(self.menu_ids.indexOf(menu_id), 1);
        };

        self.submitDetails = function()
        {
            if (self.menu_ids.length == 0)
            {
                self.err_msg = 'Please choose menu';

                return false;
            }

            $scope.block_ui();

            MenuService.submitDetails(self.menu_ids)
                .success(function(response) {
                    $location.path('/step-4');
                })
                .error(function(response) {
                    console.log(response);
                });
        };

    }

})();