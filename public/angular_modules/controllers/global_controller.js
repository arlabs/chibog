(function() {

    'use strict';

    angular.module('app')
        .controller('GlobalController',GlobalController);

    GlobalController.$inject = ['$scope', '$location'];

    function GlobalController($scope, $location)
    {
        $scope.toCheckDate = function()
        {
            $location.path('/step-2');
        };

        $scope._token = $('meta[name="_token"]').attr('content');

        // Initializes Tooltip
        $(document).tooltip({
            selector: '[data-toggle=tooltip]'
        });

        $scope.block_ui = function()
        {
            $('.loader').fadeIn();
        };

        $scope.unblock_ui = function()
        {
            $('.loader').fadeOut();
        };
    }

})();