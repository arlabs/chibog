(function() {

    'use strict';

    angular.module('app')
        .controller('CheckDateController', CheckDateController);

    CheckDateController.$inject = ['$scope', 'CheckDateService', '$location', '$rootScope'];

    function CheckDateController($scope, CheckDateService, $location, $rootScope)
    {

        var self = this;

        self.type = 'catering';

        self.skip_date = 3;

        self.hours = 4;

        self.persons = 20;

        self.set = 'A';

        console.log(self.set);

        initDateTimePicker();

        self.checkDate = function()
        {
            var event_date = $('#event_date').find('input').val();

            if (event_date === '')
            {
                self.chk_date_msg_suc = '';
                self.chk_date_msg_err = 'Please choose date';

                return false;
            }

            $scope.block_ui();

            var data = {
                date: event_date,
                hours: self.hours
            };

            CheckDateService.checkDate(data)
                .success(function(response) {
                    console.log(response);
                    $scope.start_date = response.start_date.date;
                    $scope.end_date = response.end_date.date;
                    $scope.hours = self.hours;

                    self.isAvailable = response.is_available;

                    if (response.is_available)
                    {
                        self.chk_date_msg_suc = 'Date is available';
                        self.chk_date_msg_err = '';
                    }
                    else
                    {
                        self.chk_date_msg_err = 'Date not available';
                        self.chk_date_msg_suc = '';
                    }

                    self.submit_msg = '';

                    $scope.unblock_ui();
                })
                .error(function(response) {
                    console.log(response);
                });
        };

        self.submitDetails = function()
        {
            if ( ! self.validated())
            {
                self.submit_msg = 'Fields are required or chosen date is not available.';
                return false;
            }

            if (self.type == 'catering' && self.persons < 20)
            {
                self.submit_msg = 'Minimum of 20 persons.';
                return false;
            }

            $scope.block_ui();

            $rootScope.type = self.type;

            $scope.type = self.type;
            $scope.persons = self.persons;

            var data = {
                type: $scope.type,
                start_date: ($scope.type === 'catering') ? $scope.start_date : $('#event_date').find('input').val(),
                end_date: $scope.end_date,
                hours: $scope.hours,
                persons: $scope.persons,
                set: self.set,
                _token: $scope._token
            };

            console.log(data);

            CheckDateService.submitDetails(data)
                .success(function(response) {
                    $location.path('/step-3');
                })
                .error(function(response) {
                    console.log(response)
                });
        };

        self.selectType = function(type)
        {
            self.skip_date = (type === 'catering') ? 3 : 1;

            initDateTimePicker();
        };

        // Initializes date-time picker
        function initDateTimePicker()
        {
            var now = new Date();

            var minDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + self.skip_date, 0, 0, 0, 0);

            $('#event_date').datetimepicker({
                useCurrent: false,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    next: "fa fa-chevron-right",
                    previous: "fa fa-chevron-left"
                }
            }).data("DateTimePicker").minDate(minDate);
        }

        self.validated = function()
        {
            var event_date = $('#event_date').find('input').val();

            if (self.type == 'catering') {
                return (self.type && event_date !== '' && self.hours && self.persons && self.isAvailable) ? true : false;
            }
            else {
                return (self.type && event_date !== '' && self.persons) ? true : false;
            }
        };

        self.ready = function($for)
        {
            if ($for === 'persons')
            {
                if (self.type === 'catering')
                {
                    return (self.isAvailable) ? true : false;
                }
                else
                {
                    return true;
                }
            }
        };

    }

})();