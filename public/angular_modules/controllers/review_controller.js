(function() {

    'use strict';

    angular.module('app')
        .controller('ReviewController', ReviewController);

    ReviewController.$inject = ['$scope', 'ReviewService', 'MenuService', '$window', '$location'];

    function ReviewController($scope, ReviewService, MenuService, $window, $location)
    {
        var self = this;

        ReviewService.getSessionData()
            .success(function(response) {
                self.lbl_type = response.type.toUpperCase();
                self.type = response.type;

                self.lbl_start_date = Date.parse(response.start_date);
                self.start_date = response.start_date;

                self.lbl_end_date = Date.parse(response.end_date);
                self.end_date = response.end_date;

                self.lbl_hours = response.hours;
                self.hours = response.hours;

                self.lbl_persons = response.persons;
                self.persons = response.persons;

                self.lbl_set = response.set;
                self.set = response.set;

                self.lbl_total_price = '₱' + response.total_price;

                self.menu_ids = response.menu_ids;

                MenuService.getMenus(self.menu_ids)
                    .success(function(response) {
                        self.menus = response;

                        $scope.unblock_ui();
                    })
                    .error(function(response) {
                        console.log(response);
                    });
            })
            .error(function(response) {
                console.log(response);
            });

        self.submitDetails = function()
        {
            var data = {
                type: self.type,
                start_date: self.start_date,
                end_date: self.end_date,
                hours: self.hours,
                persons: self.persons,
                set: self.set,
                menu_ids: self.menu_ids,
                _token: $scope._token
            };

            $scope.block_ui();

            ReviewService.submitDetails(data)
                .success(function(response) {
                    $location.path('/step-1');

                    $scope.unblock_ui();

                    var alert_box = $('#ajax-alert-box');
                    alert_box.removeClass('hidden');

                    alert_box.find('#alert-msg').html(
                        'Reservation has been successfully submitted.'
                    );

                    alert_box.find('#alert-btn-container').html(
                        '<a href="/reservations" class="btn btn-success">View Reservation</a>'
                    );

                    self.clearFields();
                })
                .error(function(response) {
                    console.log(response);
                });
        };

        self.clearFields = function()
        {
            self.type = '';
            self.start_date = '';
            self.end_date = '';
            self.hours = '';
            self.persons = '';
            self.set = '';
            self.menu_ids = [];
            self.menus = [];

            self.lbl_type = '';
            self.lbl_start_date = '';
            self.lbl_end_date = '';
            self.lbl_hours = '';
            self.lbl_persons = '';
            self.lbl_set = '';
            self.lbl_total_price = '';

            $('.review-footer').addClass('hidden');
        };

    }

})();