var modalTitle = $('.modal-title');
var modalSmall = $('.modal-small');
var modalContentSmall = $('.modal-content-small');

$(function() {
    // Upload image
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

        $('.btn-file').addClass('disabled');

        $('.message').removeClass('text-danger').html('<i class="fa fa-spin fa-spinner"></i>');
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var form = $(this).closest('form');

        var formData = new FormData();
        formData.append('file', $(this).prop('files')[0]);
        formData.append('_token', form.find('input[name=_token]').val());
        formData.append('path', form.find('input[name=path]').val());
        formData.append('id', form.find('input[name=id]').val() || null);

        var url = form.attr('action');
        var msg = $('.message');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false
        })
            .always(function(result) {
                console.log(result);
                if (result.success)
                {
                    msg.removeClass('text-danger');
                    msg.addClass('text-success').html(result.message);

                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
                else
                {
                    msg.removeClass('text-success');
                    msg.addClass('text-danger').html(result.message);
                }

                $('.btn-file').removeClass('disabled');
            });
    });

    // Delete package
    $('.btn-generic-delete').on('click', function(e) {
        var $this = $(this);
        var id = $this.data('id');
        var url = $this.data('url');

        addLoadingState($this,null,'Delete',true);

        $.ajax({
            url: 'delete',
            type: 'GET',
            data: {id: id, url: url}
        })
            .always(function(result) {
                modalTitle.html('Delete');
                modalContentSmall.html(result);
                modalSmall.modal('show');

                addLoadingState($this,'times','Delete',false);
            });

        e.preventDefault();
    });

    // Datatable
    $('.table-datatable').DataTable({
        "sDom": '<"top"f>t<"bottom"p><"clear">'
    });

    // Load modal to cancel reservation
    $('.btn-cancel-reservation').on('click', cancelReservation);
});

function addLoadingState(elem,icon,html,disable)
{
    if (disable) {
        elem.html('<i class="fa fa-spin fa-spinner"></i> ' + html);
        elem.addClass('disabled').attr('disabled');
    }
    else
    {
        elem.html(html);
        elem.removeClass('disabled').removeAttr('disabled');
    }
}

function cancelReservation(e)
{
    var $this = $(this);

    addLoadingState($this,null,'Cancel Reservation',true);

    $.ajax({
        url: $this.attr('href'),
        type: 'GET'
    })
        .always(function(result) {
            modalTitle.html('Cancel Reservation');
            modalContentSmall.html(result);
            modalSmall.modal('show');

            addLoadingState($this,'times','Cancel Reservation',false);
        });

    e.preventDefault();
}