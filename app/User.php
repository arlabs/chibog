<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password', 'slug'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    /**
     * Checks if user role is Admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('admin', $roles);
    }

    /**
     * Checks if user role is costumer.
     *
     * @return bool
     */
    public function isCustomer()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('customer', $roles);
    }

}
