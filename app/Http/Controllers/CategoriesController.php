<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MenuCategory;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

	public function store(Request $request)
    {
        MenuCategory::create($request->all());

        return redirect('menus/create')->with([
            'alert-level' => 'success',
            'message' => 'Category has been successfully added.'
        ]);
    }

}
