<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Menu;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {

	public function index()
    {
        $title = 'Dashboard';

        $pending = Reservation::where(function($query) {
            $query->where('status','=','pending');

            if (Auth::user()->isCustomer())
            {
                $query->where('user_id','=',Auth::id());
            }
        })->count();

        $active = Reservation::where(function($query) {
            $query->where('status','=','confirmed');

            if (Auth::user()->isCustomer())
            {
                $query->where('user_id','=',Auth::id());
            }
        })->count();

        $menus = Menu::all()->count();

        return view('dashboard.index', compact('title','pending','active','menus'));
    }

}
