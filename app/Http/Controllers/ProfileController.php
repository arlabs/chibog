<?php namespace App\Http\Controllers;

use App\Commands\ChangePassword;
use App\Http\Requests;
use App\Http\Requests\CreatePasswordRequest;
use App\Http\Requests\CreateProfileRequest;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {

	public function index()
    {
        $title = ucwords(Auth::user()->profile->full_name);
        return view('profiles.index', compact('title'));
    }

    public function updateProfile(CreateProfileRequest $request)
    {
        $profile = Profile::with(['user'])->where('user_id','=',Auth::id())->first();

        $profile->full_name = $request->get('full_name');
        $profile->address = $request->get('address');
        $profile->mobile = $request->get('mobile');
        $profile->save();

        return redirect('profile')
            ->with([
                'alert-level' => 'success',
                'message' => 'Profile has been successfully updated.'
            ]);
    }

    public function changePassword(CreatePasswordRequest $request)
    {
        if ($this->passwordMatched($request))
        {
            $this->dispatch(new ChangePassword($request));
        }
        else
        {
            return redirect()->back()->with([
                'alert-level' => 'danger',
                'message' => 'The password you have entered did not match the current password.'
            ]);
        }

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => 'Password has been successfully changed. Use your new password the next time you login.'
        ]);
    }

    private function passwordMatched(Request $request)
    {
        $password = $request->get('current_password');

        $user = User::with(['profile'])->where('id','=',Auth::id())->first();

        if (Hash::check($password, $user->password))
        {
            return true;
        } else {
            return false;
        }
    }

}
