<?php namespace App\Http\Controllers;

use App\Commands\PostDoReserve;
use App\Http\Requests;
use App\Payment;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ReservationsController extends Controller {

	public function index()
    {
        $reservations = Reservation::with(['user','menus'])->where(function($query) {
            if (Auth::user()->isCustomer())
            {
                $query->where('user_id', '=', Auth::id());
            }
        })->paginate(10);

        $title = 'Reservations';

        return view('reservations.index', compact('title','reservations'));
    }

    public function prepare()
    {
        $title = 'Create Reservation';

        return view('reservations.prepare', compact('title'));
    }

    public function checkDate(Request $request)
    {
        return Reservation::isDateAvailable($request);
    }

    public function checkDateSubmit(Request $request)
    {
        $data = [
            'type' => $request->get('type'),
            'hours' => $request->get('hours'),
            'start_date' => $request->get('start_date'),
            'persons' => $request->get('persons'),
            'set' => $request->get('set'),
            'menu_ids' => []
        ];

        $session_data = [
            'type' => $request->get('type'),
            'start_date' => date('Y-m-d H:i:s', strtotime($request->get('start_date'))),
            'end_date' => date('Y-m-d H:i:s', strtotime($request->get('end_date'))),
            'hours' => $request->get('hours'),
            'persons' => $request->get('persons'),
            'set' => $request->get('set'),
            'total_price' => Reservation::getTotalPrice($data)
        ];

        Session::put('reservation_data', $session_data);

        return ['success' => true];
    }

    public function getSessionData()
    {
        $new_sess = Session::get('reservation_data');

        $data = [
            'type' => $new_sess['type'],
            'hours' => $new_sess['hours'],
            'start_date' => $new_sess['start_date'],
            'persons' => $new_sess['persons'],
            'set' => $new_sess['set'],
            'menu_ids' => $new_sess['menu_ids']
        ];

        $new_sess['total_price'] = Reservation::getTotalPrice($data);

        Session::put('reservation_data', $new_sess);

        return Session::get('reservation_data');
    }

    public function postReserve(Request $request)
    {
        $this->dispatch(new PostDoReserve($request));

        Session::flush('reservation_data');
    }

    public function show($id)
    {
        $reservation = Reservation::with(['user','menus','payments'])->where('id','=',$id)->firstOrFail();

        $title = "Ref #: " . $reservation->reference_code;

        return view('reservations.show', compact('title','reservation'));
    }

    public function setPayment($id, Request $request)
    {
        $reservation = Reservation::with(['user','menus'])->where('id','=',$id)->firstOrFail();

        $payment = new Payment();

        $payment->reservation_id = $reservation->id;
        $payment->amount = $request->get('amount');
        $payment->save();

        return redirect()->back()
            ->with([
                'alert-level' => 'success',
                'message' => 'Payment has been successfully set.'
            ]);
    }

    public function updateStatus($id, Request $request)
    {
        $reservation = Reservation::with(['user','menus'])->where('id','=',$id)->firstOrFail();

        $reservation->status = $request->get('status');
        $reservation->notes = $request->get('notes');
        $reservation->save();

        return redirect()->back()
            ->with([
                'alert-level' => 'success',
                'message' => 'Status has been successfully updated.'
            ]);
    }

    public function getCancelReservation($id)
    {
        return view()->make('reservations.cancel', ['id' => $id])->render();
    }

    public function postCancelReservation($id)
    {
        $reservation = Reservation::with(['user','menus','payments'])->where('id','=',$id)->firstOrFail();

        $reservation->status = 'cancelled';
        $reservation->save();

        return redirect('reservations/' . $reservation->id)
            ->with([
                'alert-level' => 'success',
                'message' => 'You have successfully cancelled your reservation.'
            ]);
    }

}
