<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Menu;
use App\MenuCategory;

class PagesController extends Controller {

	public function index()
    {
        $categories = MenuCategory::all();

        $menus = Menu::all();

        return view('layouts.front', compact('categories','menus'));
    }

}
