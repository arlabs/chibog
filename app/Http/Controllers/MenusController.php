<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMenusRequest;
use App\Menu;
use App\MenuCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MenusController extends Controller {

    public function index(Request $request)
    {
        $title = 'List of Menu';

        $categories = MenuCategory::all();

        if ( ! $request->has('cat'))
        {
            return redirect('menus?cat=1');
        }

        $categ = $request->get('cat');
        $menus = Menu::with(['category'])->where('menu_category_id','=',$request->get('cat'))->get();

        return view('menus.index', compact('title','categories','menus','categ'));
    }

	public function getAll(Request $request)
    {
        $categories = MenuCategory::with('menus')->where('for','=',$request->get('type'))->get();

        return [
            'categories' => $categories
        ];
    }

    public function getMenus(Request $request)
    {
        return Menu::with(['category'])->whereIn('id',$request->get('ids'))->get();
    }

    public function submitIds(Request $request)
    {
        $new_sess = Session::get('reservation_data');
        $new_sess['menu_ids'] = $request->get('ids');

        Session::put('reservation_data', $new_sess);
    }

    public function create()
    {
        $title = 'Add New Menu';

        $categories = MenuCategory::lists('name','id');

        $menu = [];

        return view('menus.create', compact('title','categories','menu'));
    }

    public function store(CreateMenusRequest $request)
    {
        $menu = Menu::storeOrEdit($request);

        return redirect('menus?cat=' . $menu->menu_category_id)
            ->with([
                'alert-level' => 'success',
                'message' => 'Menu has been successfully added.'
            ]);
    }

    public function show($id)
    {
        $menu = Menu::with(['category','reservations'])->where('id','=',$id)->firstOrFail();

        $title = ucwords($menu->name);

        $categories = MenuCategory::lists('name','id');

        return view('menus.show', compact('title','menu', 'categories'));
    }

    public function update(CreateMenusRequest $request, $id)
    {
        Menu::with(['category','reservations'])->where('id','=',$id)->firstOrFail();

        $menu = Menu::storeOrEdit($request, 'edit', $id);

        return redirect('menus/' . $menu->id)
            ->with([
                'alert-level' => 'success',
                'message' => 'Menu has been successfully updated'
            ]);
    }

    public function destroy($id)
    {
        $menu = Menu::where('id','=',$id)->firstOrFail();

        $menu->delete();

        return redirect('menus?cat=' . $menu->menu_category_id)
            ->with([
                'alert-level' => 'success',
                'message' => 'Menu has been successfully deleted.'
            ]);
    }

}
