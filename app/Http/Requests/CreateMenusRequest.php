<?php namespace App\Http\Requests;

class CreateMenusRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'menu_category_id' => 'required|numeric',
            'name' => 'required',
            'price' => 'required|numeric'
		];
	}

}
