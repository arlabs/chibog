<?php

Route::get('/', 'PagesController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function()
{
    Route::get('delete', 'GenericController@delete');
    Route::post('upload', 'GenericController@upload');

    // Dashboard
    Route::group(['prefix' => 'dashboard'], function()
    {
        Route::get('/', 'DashboardController@index');
    });

    // Reservation
    Route::group(['prefix' => 'reservations'], function()
    {
        Route::get('/', 'ReservationsController@index');
        Route::get('prepare', 'ReservationsController@prepare');
        Route::get('{id}', 'ReservationsController@show');
        Route::post('{id}/set_payment', 'ReservationsController@setPayment');
        Route::post('{id}/update_status', 'ReservationsController@updateStatus');
        Route::group(['prefix' => 'api'], function()
        {
            Route::post('check_date', 'ReservationsController@checkDate');
            Route::post('check_date_submit', 'ReservationsController@checkDateSubmit');
            Route::get('get_session_data', 'ReservationsController@getSessionData');
        });
        Route::post('post_reserve', 'ReservationsController@postReserve');
        Route::get('{id}/get_cancel_reservation', 'ReservationsController@getCancelReservation');
        Route::post('{id}/post_cancel_reservation', 'ReservationsController@postCancelReservation');
    });

    // Categories
    Route::group(['prefix' => 'categories'], function() {
        Route::post('/', 'CategoriesController@store');
    });

    // Menu
    Route::group(['prefix' => 'menus'], function()
    {
        Route::get('/', 'MenusController@index');
        Route::get('create', 'MenusController@create');
        Route::post('/', 'MenusController@store');
        Route::get('{id}', 'MenusController@show');
        Route::post('{id}', 'MenusController@update');
        Route::delete('{id}', 'MenusController@destroy');
        Route::group(['prefix' => 'api'], function()
        {
            Route::get('get_all', 'MenusController@getAll');
            Route::post('get_menus', 'MenusController@getMenus');
            Route::post('submit_ids', 'MenusController@submitIds');
        });
    });

    // Profile
    Route::group(['prefix' => 'profile'], function()
    {
        Route::get('/', 'ProfileController@index');
        Route::post('update', 'ProfileController@updateProfile');
        Route::post('change_password', 'ProfileController@changePassword');
    });
});