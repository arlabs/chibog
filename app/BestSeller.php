<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BestSeller extends Model {

    protected $table = 'best_sellers';

    protected $fillable = ['menu_id'];

    public function menu()
    {
        return $this->belongsTo('App\Menu','menu_id');
    }

} 