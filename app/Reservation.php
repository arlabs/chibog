<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Reservation extends Model
{

    protected $fillable = ['user_id','reference_code','type','start_date','end_date','persons','total'];

    protected $dates = ['start_date','end_date'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function menus()
    {
        return $this->belongsToMany('App\Menu','menu_reservation');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public static function isDateAvailable(Request $request)
    {
        $date = $request->get('date');
        $hours = $request->get('hours');

        $dateString = date('Y-m-d',strtotime($date));
        $startDate = new Carbon($date);
        $endDate = new Carbon($date);
        $endDate->addHours($hours);

        $reservations = Reservation::with(['user','menus'])
            ->where('start_date','like','%'.$dateString.'%')
            ->where('status','=','confirmed')
            ->where('type','=','catering')
            ->where(function($query) use($startDate,$endDate) {
                $query->whereBetween('start_date', [$startDate, $endDate]);
                $query->orWhereBetween('end_date', [$startDate, $endDate]);
            })
            ->get();

        $isAvailable = $reservations->count() < 1 ? true : false;

        return [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'hours' => $hours,
            'is_available' => $isAvailable
        ];
    }

    public static function personExceeds($num_of_people)
    {
        return !! $num_of_people > config('constants.MAXIMUM_PEOPLE');
    }

    public function setReferenceCodeAttribute()
    {
        $count = Reservation::all()->count();

        $reservationID = str_pad(($count + 1),5,0,STR_PAD_LEFT);

        $this->attributes['reference_code'] = $reservationID . '-' . rand(1000,9999);
    }

    public function balance()
    {
        $total_paid = 0;
        foreach ($this->payments as $payment) {
            $total_paid += $payment->amount;
        }

        return $this->total - $total_paid;
    }

    public static function getTotalPrice($data)
    {
        $total_price = 0;

        if ($data['type'] === 'catering') {
            $day = date('D', strtotime($data['start_date']));

            if (in_array($day, config('constants.MON_THUR_ARRAY'))) {
                $total_price += config('constants.MON_THUR_PRICE');

                if ($data['hours'] > config('constants.CATERING_HOURS')) {
                    $extra_hour = $data['hours'] - config('constants.CATERING_HOURS');
                    $total_price += (config('constants.MON_THUR_PRICE_EXC') * $extra_hour);
                }
            }

            if (in_array($day, config('constants.FRI_SUN_ARRAY'))) {
                $total_price += config('constants.FRI_SUN_PRICE');

                if ($data['hours'] > config('constants.CATERING_HOURS')) {
                    $extra_hour = $data['hours'] - config('constants.CATERING_HOURS');
                    $total_price += (config('constants.FRI_SUN_PRICE_EXC') * $extra_hour);
                }
            }

            if ($data['set'] === 'A') {
                $tmp_price = config('constants.SET_A_PRICE');
            } else {
                $tmp_price = config('constants.SET_B_PRICE');
            }

            $total_price = $total_price + ($tmp_price * $data['persons']);
        }
        else
        {
            $menus = Menu::whereIn('id',$data['menu_ids'])->get();

            foreach ($menus as $menu) {
                $total_price += $menu->price;
            }
        }

        return $total_price;
    }

    public function getCancelButton()
    {
        $new_date = $this->start_date;

        if (Carbon::now()->format('Y-m-d') < $new_date->subDays(3)->format('Y-m-d'))
        {
            return '<a href="' . url('reservations/' . $this->id . '/get_cancel_reservation') . '" class="btn btn-danger btn-sm btn-cancel-reservation">Cancel Reservation</a>';
        }

        return null;
    }

} 