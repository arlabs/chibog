<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model
{
    protected $table = 'menu_categories';

    protected $fillable = ['name','for'];

    public function menus()
    {
        return $this->hasMany('App\Menu','menu_category_id');
    }
} 