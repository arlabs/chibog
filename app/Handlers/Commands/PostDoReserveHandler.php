<?php namespace App\Handlers\Commands;

use App\Commands\PostDoReserve;

use App\Events\UserHasReservedCateringOrTable;
use App\Http\Requests\Request;
use App\Reservation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class PostDoReserveHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param  PostDoReserve  $command
	 * @return void
	 */
	public function handle(PostDoReserve $command)
	{
        // Reservation
		$reservation = new Reservation();

        $reservation->user_id = Auth::user()->id;
        $reservation->reference_code = '';
        $reservation->type = $command->type;
        $reservation->start_date = date('Y-m-d H:i:s', strtotime($command->start_date));
        $reservation->end_date = date('Y-m-d H:i:s', strtotime($command->end_date));
        $reservation->persons = $command->persons;
        $reservation->set = $command->set;

        $data = [
            'type' => $command->type,
            'set' => $command->set,
            'persons' => $command->persons,
            'hours' => $command->hours,
            'start_date' => $command->start_date,
            'menu_ids' => $command->menu_ids
        ];
        $reservation->total = Reservation::getTotalPrice($data);

        $reservation->save();

        // Menu
        $reservation->menus()->attach($command->menu_ids);

        //event(new UserHasReservedCateringOrTable($reservation));
	}


}
