<?php namespace App\Handlers\Events;

use App\Events\UserHasReservedCateringOrTable;

use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailReservationNotification implements ShouldBeQueued {

    private $mailer;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserHasReservedCateringOrTable  $event
	 * @return void
	 */
	public function handle(UserHasReservedCateringOrTable $event)
	{
        $user = $event->user;

        $data = [
            'name' => ucwords($user->profile->full_name),
            'reservation' => $event->reservation
        ];

        $this->mailer->queue('emails.reservation-notification', $data, function($message) use($user)
        {
            $message->to($user->email, ucwords($user->profile->full_name))
                ->subject('Reservation Notification [Chibog]');
        });
	}

}
