<?php namespace App\Commands;

use App\Commands\Command;
use Illuminate\Http\Request;

class PostDoReserve extends Command {

    public $type;
    public $start_date;
    public $end_date;
    public $hours;
    public $persons;
    public $menu_ids;
    public $set;

    /**
     * Create a new command instance.
     *
     * @param Request $request
     */
	public function __construct(Request $request)
	{
		$this->type = $request->get('type');
		$this->start_date = $request->get('start_date');
		$this->end_date = $request->get('end_date');
		$this->hours = $request->get('hours');
		$this->persons = $request->get('persons');
		$this->set = $request->get('set');
		$this->menu_ids = $request->get('menu_ids');
	}

}
