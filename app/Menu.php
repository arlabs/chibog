<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Menu extends Model
{
    use SoftDeletes;

    protected $fillable = ['menu_category_id','name','description','price','best_seller'];

    public function category()
    {
        return $this->belongsTo('App\MenuCategory','menu_category_id');
    }

    public function reservations()
    {
        return $this->belongsToMany('App\Reservation','menu_reservation');
    }

    public function best_seller()
    {
        return $this->hasOne('App\BestSeller','menu_id');
    }

    public static function storeOrEdit(Request $request, $type = 'store', $id = null)
    {
        if ($type === 'store')
        {
            $menu = Menu::create([
                'menu_category_id' => $request->get('menu_category_id'),
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'price' => $request->get('price')
            ]);
        }
        else
        {
            $menu = Menu::with(['category','reservations'])->where('id','=',$id)->first();

            $menu->menu_category_id = $request->get('menu_category_id');
            $menu->name = $request->get('name');
            $menu->description = $request->get('description');
            $menu->price = $request->get('price');

            if ($request->get('best_seller'))
            {
                $menu->best_seller = 1;
            }

            $menu->save();
        }

        return $menu;
    }

} 