<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $fillable = ['user_id','full_name','mobile','address'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

} 