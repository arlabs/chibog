<?php namespace App\Services;

use App\Events\UserHasRegistered;
use App\Profile;
use App\User;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'full_name' => 'required|max:255',
            'mobile' => 'required|numeric',
            'address' => 'required',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$user = User::create([
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

        $profile = Profile::create([
            'user_id' => $user->id,
            'full_name' => $data['full_name'],
            'mobile' => $data['mobile'],
            'address' => $data['address']
        ]);

        $user->slug = Str::slug($user->id . ' ' . $profile->full_name);
        $user->save();

        $user->roles()->attach(2);

        // Email welcome notification
        //event(new UserHasRegistered($user));

        return $user;
	}

}
