<?php namespace App\Events;

use App\Events\Event;

use App\Reservation;
use Illuminate\Queue\SerializesModels;

class UserHasReservedCateringOrTable extends Event {

	use SerializesModels;

    public $reservation;
    public $user;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Reservation $reservation)
	{
		$this->reservation = $reservation;
        $this->user = $reservation->user;
	}

}
