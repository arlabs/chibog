<?php

return [

    'MON_THUR_PRICE' => 5000,

    'MON_THUR_PRICE_EXC' => 1000,

    'FRI_SUN_PRICE' => 7000,

    'FRI_SUN_PRICE_EXC' => 1250,

    'MAXIMUM_PEOPLE' => 20,

    'CATERING_HOURS' => 4,

    'MON_THUR_ARRAY' => ['Mon','Tue','Wed','Thu'],

    'FRI_SUN_ARRAY' => ['Fri','Sat','Sun'],

    'SET_A_PRICE' => 250,

    'SET_B_PRICE' => 350

];