var elixir = require('laravel-elixir');

elixir(function(mix) {

    mix
        .styles([
            'vendor/bootstrap/bootstrap.min.css',
            'vendor/fonts/font-awesome.min.css',
            'vendor/datetimepicker/bootstrap-datetimepicker.min.css'
        ], './public/css/libs.css')
        .styles([
            'front/isotope.css',
            'front/animate.css',
            'front/flexslider.css',
            'front/styles.css'
        ], './public/css/front.css')
        .styles([
            'back/animate.min.css',
            'back/bootstrap-switch.min.css',
            'back/checkbox3.min.css',
            'vendor/datatables/dataTables.bootstrap.css',
            'back/style.css',
            'back/flat-blue.css'
        ], './public/css/back.css');

    mix
        .scripts([
            'vendor/jquery/jquery.js',
            'vendor/bootstrap/bootstrap.js',
            'vendor/datetimepicker/moment.js',
            'vendor/datetimepicker/bootstrap-datetimepicker.min.js'
        ], './public/js/libs.js')
        .scripts([
            'front/modernizr-latest.js',
            'front/jquery.isotope.js',
            'front/jquery.nav.js',
            'front/jquery.fittext.js',
            'front/waypoints.js',
            'front/jquery.flexslider.js',
            'front/custom.js'
        ], './public/js/front.js')
        .scripts([
            'back/bootstrap-switch.min.js',
            'back/jquery.matchHeight-min.js',
            'vendor/datatables/jquery.dataTables.js',
            'vendor/datatables/dataTables.bootstrap.js',
            'back/ace/ace.js',
            'back/ace/mode-html.js',
            'back/ace/theme-github.js',
            'back/app.js'
        ], './public/js/back.js')
        .scripts([
            'vendor/angular/angular.min.js',
            'vendor/angular/angular-route.min.js',
            'angular/app.js',
            'angular/config/router.js'
        ], './public/js/angular.js');

    mix.version([
        'css/libs.css',
        'css/front.css',
        'css/back.css',
        'js/libs.js',
        'js/front.js',
        'js/back.js',
        'js/angular.js'
    ]);

});
