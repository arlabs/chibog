<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <title>Chibog</title>
        <meta name="description" content="">
        <meta name="author" content="WebThemez">

        <link href="{{ elixir('css/libs.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ elixir('css/front.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('fancybox/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css">

    </head>

    <body>

        @include('pages.nav')

        <div id="#top"></div>

        @include('pages.header_slider')

        @include('pages.about_us')

        @include('pages.menu')

        @include('pages.contact_us')

            <section class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center"> Copyright 2014 | All Rights Reserved</div>
                    </div>
                </div>
            </section>
            <a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a>

            <script src="{{ elixir('js/libs.js') }}"></script>
            <script src="{{ elixir('js/front.js') }}"></script>
            <script src="{{ asset('fancybox/jquery.fancybox.pack.js') }}"></script>
            <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}"></script>

    </body>
</html>