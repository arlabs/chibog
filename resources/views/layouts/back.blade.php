<!DOCTYPE html>
<html>

<head>
    <title>{{ $title }} | Chibog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <link href="{{ elixir('css/libs.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ elixir('css/back.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('custom/custom.css') }}" rel="stylesheet" type="text/css">
    @yield('styles')

    <script src="{{ elixir('js/libs.js') }}"></script>

</head>

<body class="flat-blue">

    <div class="card loader">
        <div class="loader-container text-center color-white">
            <div><i class="fa fa-spinner fa-pulse fa-3x"></i></div>
            <div>Loading</div>
        </div>
    </div>

    <div class="app-container expanded" ng-app="app" ng-controller="GlobalController">
        <div class="row content-container">

            @include('inclusions.back.nav')

            @include('inclusions.back.sidebar')

            <div class="container-fluid">

                <section class="side-body">
                    @if(session('message'))
                        @include('flash.template')
                    @endif

                    {{-- Ajax alert message --}}
                    <div id="ajax-alert-box" class="alert alert-success alert-dismissible hidden" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                        <strong>Success!</strong>

                        <p id="alert-msg"></p>
                        <p id="alert-btn-container"></p>
                    </div>

                    @yield('content')
                </section>

            </div>

        </div>

        @include('inclusions.back.footer')

    </div>

    @include('generic.modals')

    <script src="{{ elixir('js/back.js') }}"></script>
    <script src="{{ asset('custom/custom.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
        });
    </script>
    @yield('scripts')

</body>

</html>
