<li class="{{ Active::pattern(['users','users/*']) }} hidden">
    <a href="{{ url('users') }}">
        <i class="fa fa-users"></i> <span>Users</span>
    </a>
</li>
<li class="{{ Active::pattern(['comments','comments/*']) }} hidden">
    <a href="{{ url('comments') }}">
        <i class="fa fa-comments"></i> <span>Comments</span>
    </a>
</li>