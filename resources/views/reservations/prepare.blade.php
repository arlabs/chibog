@extends('layouts.back')

@section('content')

    <div class="row">
        <div class="col-xs-12" ng-controller="GlobalController as global">

            <div class="step">
                <ul ng-controller="NavController as nav" class="nav nav-tabs nav-justified" role="tablist">
                    <li role="step" class="@{{ nav.pathMatches('/step-1') }}">
                        <a href="#/step-1">
                            <div class="icon fa fa-check-circle-o"></div>
                            <div class="step-title">
                                <div class="title">Reservation Rules</div>
                                <div class="description">Read the reservation rules carefully</div>
                            </div>
                        </a>
                    </li>
                    <li role="step" class="@{{ nav.pathMatches('/step-2') }}">
                        <a href="#/step-2">
                            <div class="icon fa fa-check-circle-o"></div>
                            <div class="step-title">
                                <div class="title">Event Date</div>
                                <div class="description">Check your date & time availability</div>
                            </div>
                        </a>
                    </li>
                    <li role="step" class="@{{ nav.pathMatches('/step-3') }}">
                        <a href="#/step-3">
                            <div class="icon fa fa-check-circle-o"></div>
                            <div class="step-title">
                                <div class="title">Menu</div>
                                <div class="description">Select your desired menu</div>
                            </div>
                        </a>
                    </li>
                    <li role="step" class="@{{ nav.pathMatches('/step-4') }}">
                        <a href="#/step-4">
                            <div class="icon fa fa-check-circle-o"></div>
                            <div class="step-title">
                                <div class="title">Review</div>
                                <div class="description">Review your reservation</div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

            <div ng-view></div>

        </div>
    </div>

@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('js/angular.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/controllers/global_controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/controllers/nav_controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/controllers/check_date_controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/controllers/menu_controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/controllers/review_controller.js') }}"></script>

    <script type="text/javascript" src="{{ asset('angular_modules/services/check_date_service.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/services/menu_service.js') }}"></script>
    <script type="text/javascript" src="{{ asset('angular_modules/services/review_service.js') }}"></script>

    <script>
        $(function() {
            $(document).on('click', '.btn-select', function() {
                console.log('asdf');
                $(this).hide();
                $(this).parents('.caption').find('.btn-remove').show();
            });

            $(document).on('click', '.btn-remove', function() {
                $(this).hide();
                $(this).parents('.caption').find('.btn-select').show();
            });
        });
    </script>
@stop