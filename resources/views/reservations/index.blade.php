@extends('layouts.back')

@section('content')

    @if(Auth::user()->isCustomer())
       <a href="{{ url('reservations/prepare/#/step-1') }}" class="btn btn-success btn-sm pull-right on-click-disable">
           <i class="fa fa-plus"></i> Create Reservation
       </a>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">

                    @if($reservations->count())
                    <table class="table table-hover table-striped table-datatable">
                        <thead>
                        <tr>
                            <th>Ref #</th>
                            <th>Type</th>
                            <th>Costumer</th>
                            <th>No. of Persons</th>
                            <th>Event Date</th>
                            <th>Set</th>
                            <th>Total Payment</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($reservations as $reserve)
                                <tr>
                                    <td>
                                        <a href="{{ url('reservations/' . $reserve->id) }}">{{ $reserve->reference_code }}</a>
                                    </td>
                                    <td>{{ ucwords($reserve->type) }}</td>
                                    <td>{{ ucwords($reserve->user->profile->full_name) }}</td>
                                    <td>{{ $reserve->persons }}</td>
                                    <td>
                                        {{ $reserve->start_date->format('d-M-Y') }}
                                        @if($reserve->type === 'catering')
                                            ({{ $reserve->start_date->format('h:i A') }} - {{ $reserve->end_date->format('h:i A') }})
                                        @else
                                            ({{ $reserve->start_date->format('h:i A') }})
                                        @endif
                                    </td>
                                    <td>{{ $reserve->set }}</td>
                                    <td>{{ number_format($reserve->total,2) }}</td>
                                    <th>{{ ucwords($reserve->status) }}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @unless($reservations->count())
                        <h4>No reservations available</h4>
                    @endunless

                </div>
            </div>
        </div>
    </div>

@endsection