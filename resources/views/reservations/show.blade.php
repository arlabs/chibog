@extends('layouts.back')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">

                            {!! Form::open(['url' => 'reservations/' . $reservation->id . '/update_status']) !!}
                                <h3>Reservation Details:</h3>

                                <div class="row">
                                    <div class="col-sm-3"><label>Customer:</label></div>
                                    <div class="col-sm-6">
                                        <span>{{ ucwords($reservation->user->profile->full_name) }}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>Reservation Type:</label></div>
                                    <div class="col-sm-6">
                                        <span>{{ ucwords($reservation->type) }}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>Event Date:</label></div>
                                    <div class="col-sm-6">
                                        <span>
                                            {{ $reservation->start_date->format('d-M-Y') }}
                                            ({{ $reservation->start_date->format('h:i A') }} - {{ $reservation->end_date->format('h:i A') }})
                                        </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>No. of Persons:</label></div>
                                    <div class="col-sm-6">
                                        <span>{{ $reservation->persons }}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>Status:</label></div>
                                    <div class="col-sm-6">
                                        @if(Auth::user()->isAdmin())
                                            {!! Form::select('status', ['pending' => 'Pending', 'confirmed' => 'Confirmed', 'cancelled' => 'Cancelled'], $reservation->status, ['class' => 'form-control input-sm']) !!}
                                        @else
                                            {{ ucwords($reservation->status) }}
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>Set:</label></div>
                                    <div class="col-sm-6">
                                        <span>{{ $reservation->set }}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3"><label>Menu:</label></div>
                                    <div class="col-sm-6">
                                        <ol>
                                            @foreach($reservation->menus as $menu)
                                                <li>{{ ucwords($menu->name) }}</li>
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>

                                <div class="row" style="{{ Auth::user()->isAdmin() ? '' : 'display: none;' }}">
                                    <div class="col-sm-3"><label>Notes:</label></div>
                                    <div class="col-sm-6">
                                        @if(Auth::user()->isAdmin())
                                            <textarea name="notes" class="form-control input-sm" rows="5">{{ $reservation->notes }}</textarea>
                                        @else
                                            {{ $reservation->notes }}
                                        @endif
                                    </div>
                                </div>

                                <hr/>
                                @if(Auth::user()->isAdmin())
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                @else
                                    {!! $reservation->getCancelButton() !!}
                                @endif
                            {!! Form::close() !!}

                        </div>

                        <div class="col-md-6">

                            {!! Form::open(['url' => 'reservations/' . $reservation->id . '/set_payment']) !!}
                                <div class="panel fresh-color panel-primary">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <strong class="pull-right">{{ number_format($reservation->balance(),2) }}</strong>
                                            Balance
                                        </div>
                                    </div>
                                    <div class="panel-body">

                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($reservation->payments as $payment)
                                                    <tr>
                                                        <td>{{ $payment->created_at->format('d-M-Y') }}</td>
                                                        <td>{{ number_format($payment->amount,2) }}</td>
                                                    </tr>
                                                @endforeach

                                                @unless($reservation->payments->count())
                                                    <tr>
                                                        <td><span class="help-block">Customer has no payment</span></td>
                                                    </tr>
                                                @endunless
                                            </tbody>
                                            @if(Auth::user()->isAdmin())
                                                <tfoot>
                                                    <tr>
                                                        <td>Enter Amount:</td>
                                                        <td>
                                                            <input type="number" name="amount" class="form-control input-sm">
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            @endif
                                        </table>

                                        @if(Auth::user()->isAdmin())
                                            <button type="submit" class="btn btn-primary btn-sm pull-right">Pay</button>
                                        @endif

                                    </div>
                                </div>
                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection