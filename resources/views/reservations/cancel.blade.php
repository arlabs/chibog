{!! Form::open(['url' => 'reservations/'.$id.'/post_cancel_reservation']) !!}
<div class="modal-body text-center">
    Are you sure you want to cancel reservation? If you do this, you can't get the reservation back.
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" autofocus>Didn't mean to cancel.</button>
    <button type="submit" class="btn btn-primary btn-sm">Go, cancel now.</button>
</div>
{!! Form::close() !!}

<script>
    $(function() {
        $('button[type=submit]').on('click', function(e){
            $(this).addClass('disabled');
        });
    });
</script>