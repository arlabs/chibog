@extends('layouts.back')

@section('content')

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a href="{{ url('reservations') }}">
                <div class="card red summary-inline">
                    <div class="card-body">
                        <i class="icon fa fa-inbox fa-4x"></i>
                        <div class="content">
                            <div class="title">{{ $pending }}</div>
                            <div class="sub-title">Pending Reservations</div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a href="{{ url('reservations') }}">
                <div class="card yellow summary-inline">
                    <div class="card-body">
                        <i class="icon fa fa-comments fa-4x"></i>
                        <div class="content">
                            <div class="title">{{ $active }}</div>
                            <div class="sub-title">Active Reservations</div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a href="{{ url('menus') }}">
                <div class="card green summary-inline">
                    <div class="card-body">
                        <i class="icon fa fa-spoon fa-4x"></i>
                        <div class="content">
                            <div class="title">{{ $menus }}</div>
                            <div class="sub-title">Menus</div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>

@stop