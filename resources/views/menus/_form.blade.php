<div class="form-group {{ $errors->has('menu_category_id') ? 'has-error' : '' }}">
    <label>Category:</label>
    @if(Auth::user()->isAdmin())
        {!! Form::select('menu_category_id', $categories, $menu ? $menu->menu_category_id : null, ['class' => 'form-control']) !!}
    @else
        <p class="form-control">{{ $menu->category->name }}</p>
    @endif
    {!! $errors->first('menu_category_id', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label>Name:</label>
    @if(Auth::user()->isAdmin())
    {!! Form::text('name', $menu ? $menu->name : null, ['class' => 'form-control']) !!}
    @else
    <p class="form-control">{{ $menu->name }}</p>
    @endif
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    <label>Description:</label>
    @if(Auth::user()->isAdmin())
    {!! Form::textarea('description', $menu ? $menu->description : null, ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'Optional']) !!}
    @else
    <p class="form-control">{{ $menu->description }}</p>
    @endif
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    <label>Price:</label>
    @if(Auth::user()->isAdmin())
    {!! Form::number('price', $menu ? $menu->price : null, ['class' => 'form-control']) !!}
    @else
    <p class="form-control">{{ $menu->price }}</p>
    @endif
    {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    @if(Auth::user()->isAdmin())
        <div class="checkbox3 checkbox-round">
            <input type="checkbox" name="best_seller" {{ $menu && $menu->best_seller ? 'checked' : '' }} id="chk-best-seller">
            <label for="chk-best-seller">Set as Best Seller</label>
        </div>
    @else
        <p>{!! $menu->best_seller ? '<i class="fa fa-star"></i> Best Seller' : '' !!}</p>
    @endif
</div>

@if(Auth::user()->isAdmin())
<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-10">
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
    </div>
</div>
@endif