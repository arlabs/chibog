@extends('layouts.back')

@section('content')

    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-xs-4">
                    {!! Form::open(['url' => 'categories', 'class' => 'well well-sm']) !!}

                        <h4>Create new category</h4>

                        @include('categories._form')

                    {!! Form::close() !!}
                </div>

                <div class="col-xs-4 col-xs-offset-2">

                    {!! Form::open(['url' => 'menus']) !!}

                        @include('menus._form')

                    {!! Form::close() !!}

                </div>
            </div>

        </div>
    </div>

@endsection