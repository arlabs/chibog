@extends('layouts.back')

@section('content')

    @if(Auth::user()->isAdmin())
       <a href="{{ url('menus/create') }}" class="btn btn-success btn-sm pull-right on-click-disable">
           <i class="fa fa-plus"></i> Add New Menu
       </a>
    @endif
    <div class="row">
        <div class="col-xs-12">

            <ul class="nav nav-tabs" role="tablist">
                @if($categories->count())
                    @foreach($categories as $cat)
                        <li role="presentation" class="{{ $categ == $cat->id ? 'active' : '' }}">
                            <a href="{{ url('menus?cat=' . $cat->id) }}" role="tab">{{ ucwords($cat->name) }}</a>
                        </li>
                    @endforeach
                @endif
            </ul>

            <div class="card">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        @if($menus->count())
                            <div class="row">
                                @foreach($menus as $menu)
                                    <div class="col-md-3">
                                        <div class="thumbnail no-margin-bottom">
                                            <img src="/images{{ $menu->image ? '/menus/' . $menu->image : '/defaults/menu.png' }}" alt="...">
                                            <div class="caption">
                                                <h4 id="thumbnail-label">{{ ucwords($menu->name) }}</h4>
                                                <p>{{ $menu->description }}</p>
                                                @if ($menu->best_seller)
                                                    <p><i class="fa fa-check"></i> Best seller</p>
                                                @endif

                                                <a href="{{ url('menus/' . $menu->id) }}" class="btn btn-primary btn-sm" role="button">View</a>

                                                @if(Auth::user()->isAdmin())
                                                    <a href="#"
                                                        class="btn btn-danger btn-sm pull-right btn-generic-delete"
                                                        data-id="{{ $menu->id }}"
                                                        data-url="menus"
                                                    >
                                                        <i class="fa fa-times"></i> Delete
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        @unless($menus->count())
                            <h4>No menus available</h4>
                        @endunless

                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection