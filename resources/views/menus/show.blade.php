@extends('layouts.back')

@section('content')

    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-xs-4">

                    <div class="thumbnail no-margin-bottom">
                        <img src="/images{{ $menu->image ? '/menus/' . $menu->image : '/defaults/menu.png' }}" alt="...">

                        @if(Auth::user()->isAdmin())
                            {!! Form::open(['url' => 'upload', 'files' => 'true', 'id' => 'form-package-upload']) !!}
                                <span class="btn btn-default btn-file">
                                    <i class="fa fa-image"></i> Change {!! Form::file('image') !!}
                                </span>
                                {!! Form::hidden('path', 'menus') !!}
                                {!! Form::hidden('id', $menu->id) !!}
                            {!! Form::close() !!}
                            <p class="text-center message"></p>
                        @endif

                        <div class="caption">
                            <h4 id="thumbnail-label">{{ ucwords($menu->name) }}</h4>
                            <p>{{ $menu->description }}</p>
                        </div>
                    </div>

                </div>
                <div class="col-xs-4 col-xs-offset-2">

                    {!! Form::open(['url' => 'menus/' . $menu->id]) !!}

                        @include('menus._form')

                    {!! Form::close() !!}

                </div>
            </div>

        </div>
    </div>

@endsection