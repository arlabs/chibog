@extends('layouts.back')

@section('content')

    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-sm-12 col-md-3">

                    <div class="thumbnail">
                        <img src="/images{{ Auth::user()->profile->image ? '/customers/' . Auth::user()->profile->image : '/defaults/avatar.png' }}" class="img-responsive">
                        <div class="caption">
                            {!! Form::open(['url' => 'upload', 'files' => 'true']) !!}
                                <span class="btn btn-default btn-block btn-file">
                                    <i class="fa fa-image"></i> Change {!! Form::file('image') !!}
                                </span>

                                <span class="message"></span>

                                {!! Form::hidden('path', 'customers') !!}
                                {!! Form::hidden('id', Auth::id()) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-md-5">

                    {!! Form::open(['url' => 'profile/update']) !!}
                        <h3>Basic Information</h3>

                        <div class="form-group {{ $errors->has('full_name') ? 'has-error' : '' }}">
                            <label>Full Name:</label>
                            {!! Form::text('full_name', ucwords(Auth::user()->profile->full_name), ['class' => 'form-control']) !!}
                            {!! $errors->first('full_name', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                            <label>Address:</label>
                            {!! Form::text('address', ucwords(Auth::user()->profile->address), ['class' => 'form-control']) !!}
                            {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                            <label>Mobile:</label>
                            {!! Form::number('mobile', Auth::user()->profile->mobile, ['class' => 'form-control']) !!}
                            {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>

                    {!! Form::close() !!}

                </div>
                <div class="col-sm-12 col-md-4">

                    {!! Form::open(['url' => 'profile/change_password']) !!}
                        <h3>Change Password</h3>

                        <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                            <label>Current:</label>
                            {!! Form::password('current_password', ['class' => 'form-control']) !!}
                            {!! $errors->first('current_password', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                            <label>New:</label>
                            {!! Form::password('new_password', ['class' => 'form-control']) !!}
                            {!! $errors->first('new_password', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                            <label>Confirm:</label>
                            {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                            {!! $errors->first('new_password_confirmation', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Change Password</button>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>

        </div>
    </div>

@endsection