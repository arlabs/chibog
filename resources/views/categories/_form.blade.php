<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label>Name:</label>
    {!! Form::text('name', $menu ? $menu->name : null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    <label>For Type:</label>
    <div class="radio3">
        <input type="radio" name="for" id="rdo-catering" value="catering" checked>
        <label for="rdo-catering">Catering</label>
    </div>
    <div class="radio3">
        <input type="radio" name="for" id="rdo-table" value="table">
        <label for="rdo-table">Table</label>
    </div>
</div>

<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-10">
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
    </div>
</div>