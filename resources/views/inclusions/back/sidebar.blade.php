<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <div class="icon fa fa-paper-plane"></div>
                    <div class="title">Chibog</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li class="{{ Active::pattern('dashboard') }}">
                    <a href="{{ url('dashboard') }}">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="{{ Active::pattern(['reservations','reservations/*']) }}">
                    <a href="{{ url('reservations') }}">
                        <span class="icon fa fa-table"></span><span class="title">Reservations</span>
                    </a>
                </li>
                <li class="{{ Active::pattern(['menus','menus/*']) }}">
                    <a href="{{ url('menus') }}">
                        <span class="icon fa fa-file-text-o"></span><span class="title">Menu</span>
                    </a>
                </li>
                @if(Auth::user()->isAdmin())
                    @include('modules.admin')
                @else
                    @include('modules.customer')
                @endif
            </ul>
        </div>
    </nav>
</div>