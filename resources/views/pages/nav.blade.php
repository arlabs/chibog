<header class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="#" class="navbar-brand scroll-top logo  animated bounceInLeft">
                    {{--<b><i>Chibog</i></b>--}}
                    <img src="{{ asset('images/chiboglogo.png') }}" class="img-responsive" width="100" style="margin-top: -20px;"/>
                </a>
            </div>
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav" id="mainNav">
                    <li class="active" id="firstLink"><a href="#home" class="scroll-link">Home</a></li>
                    <li><a href="#aboutUs" class="scroll-link">About Us</a></li>
                    <li><a href="#work" class="scroll-link">Dishes</a></li>
                    <li><a href="#team" class="scroll-link hidden">Chef</a></li>
                    <li><a href="#contactUs" class="scroll-link">Contact Us</a></li>
                    <li><a href="{{ url('auth/login') }}" class="">Login</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>