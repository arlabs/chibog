<section id="team" class="page-section">
    <div class="container">
        <div class="heading text-center">
            <h2>Our Chef's</h2>
            <p>At variations of passages of Lorem Ipsum available, but the majority have suffered alteration..</p>
        </div>

        <div class="team-content">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="team-member pDark">
                        <div class="member-img">
                            <img class="img-responsive" src="/images/front/photo-1.jpg" alt="">
                        </div>
                        <h4>John Doe</h4>
                        <span class="pos">Manager</span>
                        <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="team-member pDark">
                        <div class="member-img">
                            <img class="img-responsive" src="/images/front/photo-2.jpg" alt="">
                        </div>
                        <h4>Larry Doe</h4>
                        <span class="pos">Senior Chef</span>
                        <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a>  </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="team-member pDark">
                        <div class="member-img">
                            <img class="img-responsive" src="/images/front/photo-3.jpg" alt="">
                        </div>
                        <h4>Ranith Kays</h4>
                        <span class="pos">Senior Chef</span>
                        <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="team-member pDark">
                        <div class="member-img">
                            <img class="img-responsive" src="/images/front/photo-4.jpg" alt="">
                        </div>
                        <h4>Joan Ray</h4>
                        <span class="pos">Chef</span>
                        <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>