<section id="aboutUs">
    <div class="container">
        <div class="heading text-center">
            <h2>Who we are?</h2>
        </div>
        <div class="row feature design">
            <div class="area1 columns right">
                <p>Chibog is a quick service grill restaurant which offers a comfortable, upscale ambiance.</p>

                <h4>Services</h4>
                <ul class="list-unstyled listArrow">
                    <li>Takes Reservations</li>
                    <li>Walk-Ins Welcome</li>
                    <li>Good For Groups</li>
                    <li>Good For Kids</li>
                    <li>Take Out and Buffet</li>
                </ul>

                <h4>Specialties</h4>
                <ul class="list-unstyled listArrow">
                    <li>Breakfast</li>
                    <li>Lunch</li>
                    <li>Dinner</li>
                    <li>Coffee and Drinks</li>
                </ul>
            </div>
            <div class="area2 columns feature-media left"> <img src="/images/front/feature-img-1.png" alt="" width="100%"> </div>
        </div>
    </div>
</section>