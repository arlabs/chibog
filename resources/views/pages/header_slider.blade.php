<section id="home">
    <div class="banner-container">
        <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                <li>
                    <img src="/images/front/slider/slide1.png" alt="" />
                    <div class="flex-caption">
                        <h3>Awarded Restaurant</h3>
                    </div>
                </li>
                <li>
                    <img src="/images/front/slider/slide2.png" alt="" />
                    <div class="flex-caption">
                        <h3>Variety of foods</h3>
                    </div>
                </li>
                <li>
                    <img src="/images/front/slider/slide3.png" alt="" />
                    <div class="flex-caption">
                        <h3>Amazing ambiance</h3>
                    </div>
                </li>
            </ul>
        </div>
        <!-- end slider -->
    </div>
    <div class="container hero-text2">
        <h3 class="hidden">Lorem ipsum dolor sit amet, ea eum labitur scsstie percipitoleat<br/> fabulas complectitur deterruisset at pro</h3>
    </div>
</section>