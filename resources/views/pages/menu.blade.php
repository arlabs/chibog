<section id="work" class="page-section page">
    <div class="container text-center">
        <div class="heading">
            <h2>Dishes</h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="portfolio">
                    <ul class="filters list-inline">
                        <li> <a data-filter=".best-seller" href="#">Best Seller</a> </li>
                        @foreach($categories as $cat)
                            <li> <a data-filter=".cat-{{ $cat->id }}" href="#">{{ ucwords($cat->name) }}</a> </li>
                        @endforeach
                    </ul>
                    <ul class="items list-unstyled clearfix animated fadeInRight showing" data-animation="fadeInRight" style="position: relative; height: 438px;">
                        @foreach($menus as $menu)
                            <li class="item cat-{{ $menu->category->id }} {{ $menu->best_seller ? 'best-seller' : '' }}" style="position: absolute; left: 0; top: 0;">
                                <a href="/images{{ $menu->image ? '/menus/' . $menu->image : '/defaults/menu.png' }}" class="fancybox">
                                    <img src="/images{{ $menu->image ? '/menus/' . $menu->image : '/defaults/menu.png' }}" alt="">
                                    <div class="overlay"> <span>{{ ucwords($menu->name) }}</span> </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>