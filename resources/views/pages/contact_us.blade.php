<section id="contactUs" class="contact-parlex">
    <div class="parlex-back">
        <div class="container">
            <div class="row">
                <div class="heading text-center">
                    <h2>Contact Us</h2>
                </div>
            </div>
            <div class="row mrgn30">
                <div class="col-sm-7">
                    <img src="/images/defaults/location.JPG" class="img-responsive">
                </div>

                <aside class="col-md-5">
                    <h3>Contact Information</h3>

                    <p>
                        Sta. Catalina Street, <br />
                        Corner San Jose Street, <br />
                        6200 Dumaguete City
                    </p>

                    <p>
                        Contact Number: (035) 522 0038 <br />
                    </p><br />

                    <h3>Business Hours:</h3>

                    <p>
                        Monday-Thursday - 7:00 am - 10:00 pm <br/>
                        Friday-Saturday - 7:00 am - 12:00 am <br/>
                        Sunday - 7:00 am - 10:00 pm
                    </p>
                </aside>
            </div>
        </div>
    </div>
</section>