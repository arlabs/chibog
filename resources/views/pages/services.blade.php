<section id="services" class="page-section colord">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center hidden"> <i class="circle"><img src="/images/front/1.png" alt="" /></i>
                <h3>Breakfast</h3>
                <p>Nullam ac rhoncus sapien, non gravida purus. Alinon elit imperdiet congue. Integer elit imperdiet congue.</p>
            </div>

            <div class="col-md-3 col-md-offset-2 text-center"><i class="circle"> <img src="/images/front/2.png" alt="" /></i>
                <h3>Catering</h3>
                <p>Nullam ac rhoncus sapien, non gravida purus. Alinon elit imperdiet congue. Integer elit imperdiet congue.</p>
            </div>

            <div class="col-md-3 col-md-offset-2 text-center"><i class="circle"> <img src="/images/front/3.png" alt="" /></i>
                <h3>Table</h3>
                <p>Nullam ac rhoncus sapien, non gravida purus. Alinon elit imperdiet congue. Integer ultricies sed elit impe.</p>
            </div>

            <div class="col-md-3 text-center hidden"><i class="circle"> <img src="/images/front/4.png" alt="" /></i>
                <h3>Party</h3>
                <p>Nullam ac rhoncus sapien, non gravida purus. Alinon elit imperdiet congue. Integer elit imperdiet conempus.</p>
            </div>
        </div>
    </div>
</section>