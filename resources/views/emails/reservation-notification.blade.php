<p>Hey, {{ $name }}!</p>

<p>
    Thank you for choosing Chibog. Your reference code is displayed below:
</p>

<h2>
    {{ $reservation['reference_code'] }}
</h2>

<p>
    Cheers,<br/>
    Chibog
</p>