<div class="alert alert-{{ session('alert-level') }} alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    @if(session('alert-level') == 'success')
        <strong>Success!</strong>
    @elseif(session('alert-level') == 'warning')
        <strong>Warning!</strong>
    @elseif(session('alert-level') == 'danger')
        <strong>Error!</strong>
    @else
        <strong>Info!</strong>
    @endif

    {!! session('message') !!}
</div>