@extends('auth.template')

@section('content')

    <div class="col-sm-12 text-center login-header">
        <h2 class="login-title">Register</h2>
    </div>
    <div class="col-sm-12">
        <div class="login-body">

            <form role="form" method="POST" action="{{ url('/auth/register') }}" class="on-submit-disable">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group has-feedback {{ $errors->has('full_name') ? 'has-error' : '' }}">
                    <input type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" placeholder="Full Name" autofocus>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    {!! $errors->first('full_name', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}">
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                    {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group has-feedback {{ $errors->has('address') ? 'has-error' : '' }}">
                    <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address">
                    <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password"/>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-pencil"></i> Register</button>
                </div>
            </form>

            <div class="text-center">
                <p>- Already got an account? -</p>
                <a href="{{ url('auth/login') }}" class="btn btn-success btn-block btn-flat">Login</a>
            </div>

        </div>
    </div>

@endsection
