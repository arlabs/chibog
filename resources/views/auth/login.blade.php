@extends('auth.template')

@section('content')

    <div class="col-sm-12 text-center login-header">
        <h2 class="login-title">Login</h2>
    </div>
    <div class="col-sm-12">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-body">

            <form role="form" method="POST" action="{{ url('/auth/login') }}" class="on-submit-disable">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" autofocus/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
                </div>
            </form>

            <div class="text-center">
                <p>- Got no account? -</p>
                <a href="{{ url('auth/register') }}" class="btn btn-success btn-block btn-flat">Register</a>
            </div>

        </div>
        <div class="login-footer">
            <span class="text-right"><a class="text-center" href="{{ url('/password/email') }}">Forgot your password?</a></span>
        </div>
    </div>

@endsection
