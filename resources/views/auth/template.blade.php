<!DOCTYPE html>
<html>

<head>
    <title>Chibog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <link href="{{ elixir('css/libs.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ elixir('css/back.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ elixir('js/libs.js') }}"></script>

</head>

<body class="flat-blue login-page">

    <div class="container">
        <div class="login-box">
            <div class="login-form row">

                @yield('content')

            </div>
        </div>
    </div>

    <script src="{{ elixir('js/back.js') }}"></script>

</body>

</html>
