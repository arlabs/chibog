(function() {

    'use strict';

    angular.module('app').config(Config);

    Config.$inject = ['$routeProvider'];

    function Config($routeProvider) {
        $routeProvider.
            when('/step-1', {
                templateUrl: '/views/reservations/step1.html'
            }).
            when('/step-2', {
                templateUrl: '/views/reservations/step2.html'
            }).
            when('/step-3', {
                templateUrl: '/views/reservations/step3.html'
            }).
            when('/step-4', {
                templateUrl: '/views/reservations/step4.html'
            }).
            otherwise({
                redirectTo: '/step-1'
            });
    }

})();